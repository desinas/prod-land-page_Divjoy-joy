import React, { useState } from "react";
import { Link } from "react-router-dom";
import useFakeAuth from "./../../util/use-fake-auth.js";
import "./styles.scss";

function Navbar(props) {
  const [menuOpen, setMenuOpen] = useState(false);
  const [auth, signin, signout] = useFakeAuth();

  const logoImg = props.dark
    ? "https://bulma.io/images/bulma-logo-white.png"
    : "https://bulma.io/images/bulma-logo.png";

  return (
    <nav
      className={
        "NavbarComponent navbar is-spaced" + (props.dark ? " is-dark" : "")
      }
    >
      <div className="container">
        <div className="navbar-brand">
          <div className="navbar-item">
            <Link to="/">
              <img className="image" src={logoImg} width={112} height={28} />
            </Link>
          </div>
          <div
            className={"navbar-burger burger" + (menuOpen ? " is-active" : "")}
            onClick={() => setMenuOpen(!menuOpen)}
          >
            <span />
            <span />
            <span />
          </div>
        </div>
        <div className={"navbar-menu" + (menuOpen ? " is-active" : "")}>
          <div className="navbar-end">
            {auth === true && (
              <div className="navbar-item has-dropdown is-hoverable">
                <Link className="navbar-link" to="/">
                  Account
                </Link>
                <div className="navbar-dropdown is-boxed">
                  <Link className="navbar-item" to="/user/gabe">
                    Profile
                  </Link>
                  <a className="navbar-item" onClick={e => signout()}>
                    Sign out
                  </a>
                </div>
              </div>
            )}

            {auth === false && (
              <Link className="navbar-item" to="/signin">
                Sign in
              </Link>
            )}
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;

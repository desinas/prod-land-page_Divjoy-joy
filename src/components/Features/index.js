import React from "react";
import "./styles.scss";

function Features(props) {
  return (
    <div className="Features columns is-multiline is-centered is-gapless">
      {props.items.map((item, index) => (
        <div className="Features__column column is-half" key={index}>
          <div className="Features__no-classname has-text-centered">
            <figure className="Features__image image">
              <img src={item.image} />
            </figure>
            <h1 className="title is-4 is-spaced">{item.title}</h1>
            <h2 className="subtitle is-6">{item.body}</h2>
          </div>
        </div>
      ))}
    </div>
  );
}

export default Features;

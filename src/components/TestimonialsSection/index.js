import React from "react";
import Testimonials from "./../Testimonials";
import "./styles.scss";

function TestimonialsSection(props) {
  return (
    <section className="TestimonialsSection section is-medium">
      <div className="container">
        <header className="section-header">
          <h1 className="title has-text-centered has-text-white">
            Here's what people are saying
          </h1>
        </header>
        <Testimonials
          items={[
            {
              avatar: "http://i.pravatar.cc/150?img=68",
              name: "John Smith",
              quote:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.",
              company: "Company"
            },
            {
              avatar: "http://i.pravatar.cc/150?img=35",
              name: "Lisa Zinn",
              quote:
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum consequatur numquam aliquam tenetur ad amet inventore hic beatae, quas accusantium perferendis sapiente explicabo, corporis totam!",
              company: "Company"
            },
            {
              avatar: "http://i.pravatar.cc/150?img=16",
              name: "Diana Low",
              quote:
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum consequatur numquam aliquam tenetur ad amet inventore hic beatae.",
              company: "Company"
            }
          ]}
        />
      </div>
    </section>
  );
}

export default TestimonialsSection;

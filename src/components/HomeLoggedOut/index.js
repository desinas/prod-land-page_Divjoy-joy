import React from "react";
import NavHero from "./../NavHero";
import FeaturesSection from "./../FeaturesSection";
import ClientsSection from "./../ClientsSection";
import TestimonialsSection from "./../TestimonialsSection";
import PricingSection from "./../PricingSection";
import Divider from "./../Divider";
import Footer from "./../Footer";
import "./styles.scss";

function HomeLoggedOut(props) {
  return (
    <>
      <NavHero />
      <FeaturesSection />
      <ClientsSection />
      <TestimonialsSection />
      <PricingSection />
      <Divider />
      <Footer />
    </>
  );
}

export default HomeLoggedOut;

import React from "react";
import Home from "./../Home";
import Signin from "./../Signin";
import Signup from "./../Signup";
import Forgotpass from "./../Forgotpass";
import Changepass from "./../Changepass";
import UserUsername from "./../UserUsername";
import { Switch, Route, Router } from "react-router-dom";
import navigate from "./../../util/navigate.js";
import "./styles.scss";

function App(props) {
  return (
    <Router history={navigate}>
      <Switch>
        <Route exact path="/" component={Home} />

        <Route exact path="/signin" component={Signin} />

        <Route exact path="/signup" component={Signup} />

        <Route exact path="/forgotpass" component={Forgotpass} />

        <Route exact path="/changepass" component={Changepass} />

        <Route exact path="/user/:username" component={UserUsername} />

        <Route
          component={({ location }) => {
            return (
              <div style={{ padding: "20px" }}>
                The page <code>{location.pathname}</code> could not be found 😿
              </div>
            );
          }}
        />
      </Switch>
    </Router>
  );
}

export default App;

import React from "react";
import Navbar from "./../Navbar";
import Footer from "./../Footer";
import "./styles.scss";

function UserUsername({ match: { params }, ...props }) {
  // Grab user data with uid
  // Normally you'd fetch via an API/DB call
  const users = {
    gabe: {
      username: "gabe",
      displayName: "Gabe Ragland"
    }
  };

  const user = users[params.username];

  return (
    <>
      <Navbar />
      <div className="NoComponentName__replace-this section is-large has-text-centered">
        <div className="container">
          {user && (
            <>
              <h1 className="title is-spaced">User Profile</h1>
              <h2 className="subtitle">Name: {user.displayName}</h2>
            </>
          )}

          {!user && (
            <>No user with the username "{params.username}" was found :(</>
          )}
        </div>
      </div>
      <Footer />
    </>
  );
}

export default UserUsername;

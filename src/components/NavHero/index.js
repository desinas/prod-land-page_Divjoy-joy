import React from "react";
import Navbar from "./../Navbar";
import "./styles.scss";

function NavHero(props) {
  return (
    <section className="NavHero hero">
      <div className="hero-head">
        <Navbar dark={true} />
        <div className="hero-body">
          <div className="container has-text-centered">
            <header className="section-header">
              <h1 className="title is-spaced is-1 is-size-3-mobile has-text-weight-bold">
                Your landing page title here
              </h1>
              <p className="NavHero__subtitle subtitle">
                This landing page is perfect for showing off your awesome
                product and driving people to sign up for a paid plan.
              </p>
            </header>
            <button
              className="button is-primary is-medium"
              onClick={() => {
                const el = document.getElementById("pricing");
                el.scrollIntoView({ behavior: "smooth", block: "start" });
              }}
            >
              Start Free Trial
            </button>
          </div>
        </div>
      </div>
    </section>
  );
}

export default NavHero;
